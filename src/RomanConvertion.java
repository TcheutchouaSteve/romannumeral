import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/*
Created by Gil
*/

public class RomanConvertion {
	
	//defining variables for tests 
	private final String roman64 = "LXIV" ;
	private final String roman226 = "CCXXVI" ;
	private final String roman900 = "CM" ;
	//private final String roman998 = "CMXCVIII" ;
	private final String roman1712 = "MDCCXII" ;
	
	
	
	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(RomanConvertion.class);
	      for (Failure failure : result.getFailures()) {
	         System.out.println(failure.toString());
	      }
	      System.out.println(result.wasSuccessful());	
	}
	
	public static  String getRoman(int number) {
		
		// Stores the standard roman numeral Strings 
	    String roman[] = {"M","XM","CM","D","XD","CD","C","XC","L","XL","X","IX","V","IV","I"};
	    //create a corresponding array with respective decimal values as in roman
	    int arab[] = {1000, 990, 900, 500, 490, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
	    StringBuilder result = new StringBuilder();
	    
	    // parse through the number to be converted to convert the number to roman numeral
	    int i = 0;
	    while (number > 0 || arab.length == (i - 1)) {
	        while ((number - arab[i]) >= 0) {
	            number -= arab[i];
	            result.append(roman[i]);
	        }
	        i++;
	    }
	    return result.toString();
	}
	
	// performing tests for the different variables 
	
	@Test
	public void test() {
		RomanConvertion rmConv = new RomanConvertion();
		assertTrue(roman64.equals(rmConv.getRoman(64)));
		
		//assertTrue(roman64.equals(getRoman));
	}
	
	@Test 
	public void test266(){
		assertTrue(roman226.equals(this.getRoman(226)));
	}
	
	@Test 
	public void test900(){
		assertTrue(roman900.equals(this.getRoman(900)));
	}
	
	
	@Test 
	public void test1712(){
		assertTrue(roman1712.equals(this.getRoman(1712)));
	}
	
	@Test 
	public void testFail988(){
		assertTrue(roman226.equals(this.getRoman(988)));
	}
}
